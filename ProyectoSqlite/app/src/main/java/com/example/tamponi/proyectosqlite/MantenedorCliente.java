package com.example.tamponi.proyectosqlite;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Iterator;
import java.util.List;

public class MantenedorCliente extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mantenedor_cliente);
    }

    public void guardar(View view) {
        try {
            EditText auxRut = (EditText) findViewById(R.id.txtRut);
            EditText auxNombre = (EditText) findViewById(R.id.txtNombre);

            Cliente auxCliente = new Cliente();
            auxCliente.setRut(auxRut.getText().toString());
            auxCliente.setNombre(auxNombre.getText().toString());

            NegocioMantenedorCliente auxNegocio = new NegocioMantenedorCliente(this);

            auxNegocio.insertarDatos(auxCliente);
            this.mensaje("Cliente Guardado");
            auxRut.setText("");
            auxNombre.setText("");
            auxRut.requestFocus();

        } catch (Exception ex) {
            this.mensaje("Error al guardar el Cliente " + ex.getMessage());

        }


    } //Fin guardar

    public void mostrar(View view) {

        NegocioMantenedorCliente auxNegocio = new NegocioMantenedorCliente(this);

        List<Cliente> auxLista = auxNegocio.retornaClientes();

        String[] listaString = new String[auxLista.size()];

        Iterator iter = auxLista.iterator();

        int pos = 0;

        while (iter.hasNext()) {
            Cliente auxCliente = new Cliente();
            auxCliente = (Cliente) iter.next();
            listaString[pos] = auxCliente.getRut() + " " + auxCliente.getNombre();
            pos++;
        }

        ListView auxListView = (ListView) findViewById(R.id.listaCliente);

        auxListView.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaString));


    } //Fin mostrar


    public void mensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

} //Fin Clase
