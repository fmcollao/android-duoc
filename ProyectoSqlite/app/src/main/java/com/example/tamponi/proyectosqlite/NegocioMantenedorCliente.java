package com.example.tamponi.proyectosqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tamponi on 02-09-2017.
 */

public class NegocioMantenedorCliente extends SQLiteOpenHelper {
    private static final int VERSION_BASEDATOS = 1;
    private static final String NOMBRE_BASEDATO = "prueba.db";
    private static final String TABLA_CLIENTE = "CREATE TABLE cliente " +
            "(rut TEXT PRIMARY KEY,nombre TEXT) ";

    public NegocioMantenedorCliente(Context context) {
        super(context, NOMBRE_BASEDATO, null, VERSION_BASEDATOS);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_CLIENTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_CLIENTE);
        onCreate(db);
    }

    //Metodo para insertar datos

    public void insertarDatos(Cliente cliente) {
        SQLiteDatabase db = getWritableDatabase();

        if (db != null) {
            db.execSQL("INSERT INTO cliente "
                    + " (rut,nombre) "
                    + " VALUES ( "
                    + "'" + cliente.getRut()
                    + "','" + cliente.getNombre()
                    + "');");

        }
        db.close();

    } //Fin insertar

    //Metodo Actualizar Cliente

    public void actualizarCliente(Cliente cliente) {
        SQLiteDatabase db = getWritableDatabase();

        if (db != null) {
            db.execSQL("UPDATE cliente"
                    + " SET"
                    + " nombre =  '" + cliente.getNombre()
                    + "' WHERE rut = '" + cliente.getRut() + "';");

        }
        db.close();
    } //Fin actualizar Cliente

    //Metodo Eliminar Cliente

    public void eliminarCliente(String rut) {
        SQLiteDatabase db = getWritableDatabase();

        if (db != null) {
            db.execSQL("DELETE FROM cliente"
                    + " WHERE rut = '" + rut + "';");

        }
        db.close();
    } //Fin Eliminar Cliente


    public void eliminarTodosLosCliente() {
        SQLiteDatabase db = getWritableDatabase();

        if (db != null) {
            db.execSQL("DELETE FROM cliente;");
        }
        db.close();
    } //Fin Eliminar Cliente

    //Retorna o consuta Clientes
    public List<Cliente> retornaClientes() {
        SQLiteDatabase db = getWritableDatabase();
        List<Cliente> auxListaCliente = new ArrayList<>();

        Cursor auxCursor = db.rawQuery("SELECT * FROM cliente;", null);

        auxCursor.moveToFirst();

        do {
            Cliente auxCliente = new Cliente();
            auxCliente.setRut(auxCursor.getString(0));
            auxCliente.setNombre(auxCursor.getString(1));
            auxListaCliente.add(auxCliente);


        } while (auxCursor.moveToNext());

        auxCursor.close();
        db.close();
        return auxListaCliente;

    } //Fin retorna Cliente


    //Metodo Buscar cliente
    public Cliente buscarCliente(String rut) {
        SQLiteDatabase db = getWritableDatabase();
        Cliente auxCliente = new Cliente();

        Cursor auxCursor = db.rawQuery("SELECT * FROM cliente "
                + " WHERE rut = '" + rut + "';", null);

        auxCursor.moveToFirst();

        if (auxCursor != null) {
            auxCliente.setRut(auxCursor.getString(0));
            auxCliente.setNombre(auxCursor.getString(1));

        } else {
            auxCliente.setRut("");
            auxCliente.setNombre("");
        }
        auxCursor.close();
        db.close();
        return auxCliente;

    } //Fin buscar Cliente


} //Fin clase
