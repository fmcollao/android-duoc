package com.example.tamponi.proyectosqlite;

/**
 * Created by tamponi on 02-09-2017.
 */

public class Cliente {
    private String rut;
    private String nombre;


    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
