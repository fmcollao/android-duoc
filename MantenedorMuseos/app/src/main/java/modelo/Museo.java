package modelo;

/**
 * Created by Francisco Mora on 23-09-2017.
 */

public class Museo {
    private int id;
    private String nombre;
    private String direccion;
    private String sitioWeb;
    private String descripcion;
    private String horario;

    public Museo(int id, String nombre, String direccion, String sitioWeb, String descripcion, String horario) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.sitioWeb = sitioWeb;
        this.descripcion = descripcion;
        this.horario = horario;
    }

    public Museo(String nombre, String direccion, String sitioWeb, String descripcion, String horario) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.sitioWeb = sitioWeb;
        this.descripcion = descripcion;
        this.horario = horario;
    }

    public Museo() {
    }

    @Override
    public String toString() {
        return "Museo{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", sitioWeb='" + sitioWeb + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", horario='" + horario + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSitioWeb() {
        return sitioWeb;
    }

    public void setSitioWeb(String sitioWeb) {
        this.sitioWeb = sitioWeb;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
