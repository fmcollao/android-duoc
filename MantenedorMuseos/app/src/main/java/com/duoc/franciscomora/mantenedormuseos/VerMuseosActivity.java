package com.duoc.franciscomora.mantenedormuseos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import database.MantenedorMuseo;
import modelo.Museo;

public class VerMuseosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_museos);
    }

    public void mostrarTodos(View view) {
        MantenedorMuseo auxMantenderMuseo = new MantenedorMuseo(this);
        ArrayList<Museo> auxLista = auxMantenderMuseo.verMuseos();
        String[] listaString = new String[auxLista.size()];
        Iterator iter = auxLista.iterator();

        int posicion = 0;

        while (iter.hasNext()) {
            Museo auxMuseo = new Museo();
            auxMuseo = (Museo) iter.next();
            listaString[posicion] = auxMuseo.getNombre() + " " + auxMuseo.getDireccion();
            posicion++;
        }

        ListView auxListView = (ListView) findViewById(R.id.lvVerMuseos);
        auxListView.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaString));
    }
}
