package com.duoc.franciscomora.mantenedormuseos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void lanzarMantenedorInstitucion(View view) {
        Intent lanzaMantenedorMuseos = new Intent(this, MuseosActivity.class);
        startActivity(lanzaMantenedorMuseos);
    }

    public void lanzarMantenedorVisitantes(View view) {
        Intent lanzaMantenedorVisitantes = new Intent(this, VisitantesActivity.class);
        startActivity(lanzaMantenedorVisitantes);
    }
}
