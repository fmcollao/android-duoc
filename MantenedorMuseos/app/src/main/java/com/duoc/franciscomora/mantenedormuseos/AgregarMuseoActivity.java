package com.duoc.franciscomora.mantenedormuseos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import database.MantenedorMuseo;
import modelo.Museo;

public class AgregarMuseoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_museo);
    }

    public void agregarNuevoMuseo(View view) {
        try {
            EditText auxNombre = (EditText) findViewById(R.id.txtNombreMuseo);
            EditText auxDireccion = (EditText) findViewById(R.id.txtDireccionMuseo);
            EditText auxSitioWeb = (EditText) findViewById(R.id.txtSitioWebMuseo);
            EditText auxHorario = (EditText) findViewById(R.id.txtHorarioMuseo);
            EditText auxDescripcion = (EditText) findViewById(R.id.txtDescripcionMuseo);

            Museo nuevoMuseo = new Museo();
            nuevoMuseo.setNombre(auxNombre.toString());
            nuevoMuseo.setDireccion(auxDireccion.toString());
            nuevoMuseo.setSitioWeb(auxSitioWeb.toString());
            nuevoMuseo.setDescripcion(auxDescripcion.toString());
            nuevoMuseo.setHorario(auxHorario.toString());

            MantenedorMuseo agregaMuseo = new MantenedorMuseo(this);
            agregaMuseo.agregarMuseo(nuevoMuseo);
            this.mensaje("Nuevo Museo Ingresado Correctamente..");
            auxNombre.setText("");
            auxDireccion.setText("");
            auxSitioWeb.setText("");
            auxHorario.setText("");
            auxDescripcion.setText("");
            auxNombre.requestFocus();


        }catch(Exception ex){
            this.mensaje("Error al guardar nuevo Museo: " + ex.getMessage());
        }
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
