package com.duoc.franciscomora.mantenedormuseos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MuseosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museos);
    }

    public void ver(View view) {
        Intent lanzaVerMuseos = new Intent(this, VerMuseosActivity.class);
        startActivity(lanzaVerMuseos);
    }

    public void agregar(View view) {
        Intent lanzaAgregarMuseos = new Intent(this, AgregarMuseoActivity.class);
        startActivity(lanzaAgregarMuseos);
    }
}
