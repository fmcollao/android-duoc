package database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import modelo.Museo;
import modelo.Visitante;

/**
 * Created by Francisco Mora on 04-10-2017.
 */

public class MantenedorVisitante extends SQLiteOpenHelper{
    private static final int VERSION_BASE_DATOS = 1;
    private static final String NOMBRE_BD = "mantenedores.db";
    private static final String TABLA_VISITANTE = "CREATE TABLE visitante" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, run TEXT, nombre TEXT, apellido TEXT, direccion TEXT, telefono TEXT, email TEXT)";

    public MantenedorVisitante(Context context) {
        super(context, NOMBRE_BD, null, VERSION_BASE_DATOS);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_VISITANTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLA_VISITANTE);
        onCreate(db);
    }

    public void agregarVisitante(Visitante visitante)
    {
        SQLiteDatabase db = getWritableDatabase();
        try {
            if (db != null){
                db.execSQL("INSERT INTO visitante (run,nombre,apellido,direccion,telefono,email) VALUES (" +
                        " ''" + visitante.getRun() +
                        " ''" + visitante.getNombre() +
                        " ''" + visitante.getApellido() +
                        " ''" + visitante.getDireccion() +
                        " ''" + visitante.getTelefono() +
                        " ''" + visitante.getEmail() +
                        ");");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            db.close();
        }
    }

    public void editarVisitante(Visitante visitante)
    {
        SQLiteDatabase db= getWritableDatabase();

        try {
            if (db != null)
            {
                db.execSQL("UPDATE visitante SET"
                        +   " run =  '" + visitante.getRun()
                        +   " nombre =  '" + visitante.getNombre()
                        +   " apellido =  '" + visitante.getApellido()
                        +   " direccion =  '" + visitante.getDireccion()
                        +   " telefono =  '" + visitante.getTelefono()
                        +   " email =  '" + visitante.getEmail()
                        +   "' WHERE id = '"+ visitante.getId() +"';");

            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            db.close();
        }
    }

    public void eliminarVisitante(int id)
    {
        SQLiteDatabase db= getWritableDatabase();

        try {
            if (db != null)
            {
                db.execSQL("DELETE FROM visitante WHERE id = '" + id + "';");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            db.close();
        }
    }

    public List<Visitante> verVisitantes()
    {
        SQLiteDatabase db= getWritableDatabase();
        ArrayList<Visitante> auxListaVisitantes = new ArrayList<Visitante>();
        Cursor auxCursor = db.rawQuery("SELECT * FROM visitante;", null);
        auxCursor.moveToFirst();

        try {
            do
            {
                Visitante auxVisitante = new Visitante();
                auxVisitante.setRun(auxCursor.getString(0));
                auxVisitante.setNombre(auxCursor.getString(1));
                auxVisitante.setApellido(auxCursor.getString(2));
                auxVisitante.setDireccion(auxCursor.getString(3));
                auxVisitante.setTelefono(auxCursor.getString(4));
                auxVisitante.setEmail(auxCursor.getString(5));
                auxListaVisitantes.add(auxVisitante);
            } while(auxCursor.moveToNext());
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            auxCursor.close();
            db.close();
        }

        return auxListaVisitantes;
    }
}
