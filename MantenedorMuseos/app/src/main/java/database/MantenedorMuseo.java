package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by Francisco Mora on 23-09-2017.
 */

public class MantenedorMuseo extends SQLiteOpenHelper{
    private static final int VERSION_BASE_DATOS = 1;
    private static final String NOMBRE_BD = "mantenedores.db";
    private static final String TABLA_MUSEO = "CREATE TABLE museo (id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, direccion TEXT, sitioWeb TEXT, descripcion TEXT, horario TEXT)";                                               "";

    public MantenedorMuseo(Context context) {
        super(context, NOMBRE_BD, null, VERSION_BASE_DATOS);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_MUSEO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLA_MUSEO);
        onCreate(db);
    }
}
