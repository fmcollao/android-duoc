package com.duoc.franciscomora.mantenedores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainVisitantesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_visitantes);
    }

    public void mostrarRegistrarVisitante(View view) {
        Intent registroVisitante = new Intent(this, RegistrarVisitanteActivity.class);
        startActivity(registroVisitante);
    }

    public void mostrarConsultarVisitante(View view) {
        Intent consultarVisitante = new Intent(this, ConsultaVisitanteActivity.class);
        startActivity(consultarVisitante);
    }

    public void mostrarListadoVisitantes(View view) {
        Intent listarVisitante = new Intent(this, ListadoVisitantesActivity.class);
        startActivity(listarVisitante);
    }
}
