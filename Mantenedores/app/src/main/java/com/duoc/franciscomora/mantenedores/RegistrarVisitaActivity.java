package com.duoc.franciscomora.mantenedores;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;

import database.ConexionHelper;
import modelo.Museo;
import modelo.Visitante;
import utilidades.Utilidades;

public class RegistrarVisitaActivity extends AppCompatActivity {
    Spinner spMuseo;
    Spinner spVisitante;
    EditText txtFecha;
    EditText txtHora;
    int sYear;
    int sMonth;
    int sDay;
    int sHora;
    int sMinutos;
    private static final int DIA_ID = 0;
    ArrayList<String> listadoMuseos;
    ArrayList<Museo> listaMuseo;
    ArrayList<String> listadoVisitantes;
    ArrayList<Visitante> listaVisitante;
    ConexionHelper conn;
    boolean camposOK = false;

    Calendar calendario = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_visita);
        conn = new ConexionHelper(this, "mantenedores" , null, 1);

        spMuseo = (Spinner) findViewById(R.id.spMuseo);
        spVisitante = (Spinner) findViewById(R.id.spVisitante);
        sDay = calendario.get(Calendar.DAY_OF_MONTH);
        sMonth = calendario.get(Calendar.MONTH);
        sYear = calendario.get(Calendar.YEAR);
        sMonth = sMonth + 1;
        sHora = calendario.get(Calendar.HOUR_OF_DAY);
        sMinutos = calendario.get(Calendar.MINUTE);
        txtFecha = (EditText) findViewById(R.id.txtFecha);
        txtHora = (EditText) findViewById(R.id.txtHora);

        consultarListadoMuseos();
        consultarListadoVisitantes();

        ArrayAdapter<CharSequence> adaptadorMuseos = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listadoMuseos);
        spMuseo.setAdapter(adaptadorMuseos);
        ArrayAdapter<CharSequence> adaptadorVisitantes = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listadoVisitantes);
        spVisitante.setAdapter(adaptadorVisitantes);
    }

    private void consultarListadoVisitantes() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Visitante visitante = null;
        listaVisitante = new ArrayList<Visitante>();
        String sql = "SELECT * FROM " + Utilidades.TABLA_VISITANTE;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            visitante = new Visitante();
            visitante.setNombre(cursor.getString(2));
            visitante.setApellido(cursor.getString(3));

            listaVisitante.add(visitante);
        }

        obtenerListaVisitante();
    }

    private void obtenerListaVisitante() {
        listadoVisitantes = new ArrayList<String>();

        for (int i=0; i < listaVisitante.size();i++){
            listadoVisitantes.add(listaVisitante.get(i).getNombre() + " " + listaVisitante.get(i).getApellido());
        }
    }

    private void consultarListadoMuseos() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Museo museo = null;
        listaMuseo = new ArrayList<Museo>();
        String sql = "SELECT * FROM " + Utilidades.TABLA_MUSEO;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            museo = new Museo();
            museo.setNombre(cursor.getString(1));

            listaMuseo.add(museo);
        }

        obtenerListaMuseos();
    }

    private void obtenerListaMuseos() {
        listadoMuseos = new ArrayList<String>();

        for (int i=0; i < listaMuseo.size();i++){
            listadoMuseos.add(listaMuseo.get(i).getNombre());
        }
    }

    public void seleccionarFecha(View view) {
        DatePickerDialog dpDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                txtFecha.setText(day + "-" + month + "-" + year);
            }
        }, sYear, sMonth, sDay);

        dpDialog.show();
    }

    public void seleccionarHora(View view) {
        TimePickerDialog tpDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hora, int minutos) {
                txtHora.setText(hora + ":" + minutos);
            }
        }, sHora, sMinutos, false);

        tpDialog.show();
    }

    public void registrarVisita(View view) {
        SQLiteDatabase db = conn.getWritableDatabase();

        if (validarCamposVacios()){
            ContentValues valores = new ContentValues();
            valores.put(Utilidades.CAMPO_NOMBRE_MUSEO_REGISTRADO, spMuseo.getSelectedItem().toString());
            valores.put(Utilidades.CAMPO_NOMBRE_VISITANTE_REGISTRADO, spVisitante.getSelectedItem().toString());
            valores.put(Utilidades.CAMPO_FECHA_VISITA, txtFecha.getText().toString());
            valores.put(Utilidades.CAMPO_HORA_VISITA, txtHora.getText().toString());

            Long idRegistro = db.insert(Utilidades.TABLA_VISITAS, Utilidades.CAMPO_ID_VISITAS, valores);
            mensaje("VISITA REGISTRADA CORRECTAMENTE");
        }
    }

    private boolean validarCamposVacios() {
        if (TextUtils.isEmpty(txtFecha.getText().toString().trim()) && txtFecha.getText().toString().trim().length() == 0){
            txtFecha.setError("Campo Fecha es obligatorio");
            txtFecha.requestFocus();
        }else if (TextUtils.isEmpty(txtHora.getText().toString().trim()) && txtHora.getText().toString().trim().length() == 0){
            txtHora.setError("Campo Hora es obligatorio");
            txtHora.requestFocus();
        }else {
            camposOK = true;
        }

        return camposOK;
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
