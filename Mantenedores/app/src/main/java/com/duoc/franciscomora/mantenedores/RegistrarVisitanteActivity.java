package com.duoc.franciscomora.mantenedores;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import database.ConexionHelper;
import utilidades.Utilidades;

public class RegistrarVisitanteActivity extends AppCompatActivity {
    EditText run;
    EditText nombre;
    EditText apellido;
    EditText email;
    EditText telefono;
    Boolean camposOK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_visitante);

        run = (EditText) findViewById(R.id.txtRunVisitante);
        nombre = (EditText) findViewById(R.id.txtNombreVisitante);
        apellido = (EditText) findViewById(R.id.txtApellidoVisitante);
        email = (EditText) findViewById(R.id.txtEmailVisitante);
        telefono = (EditText) findViewById(R.id.txtTelefonoVisitante);
    }

    public void registrarVisitante(View view) {
        ConexionHelper conn = new ConexionHelper(this, "mantenedores" , null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();

        if (validarCamposVacios()){
            ContentValues valores = new ContentValues();
            valores.put(Utilidades.CAMPO_RUN_VISITANTE, run.getText().toString());
            valores.put(Utilidades.CAMPO_NOMBRE_VISITANTE, nombre.getText().toString());
            valores.put(Utilidades.CAMPO_APELLIDO_VISITANTE, apellido.getText().toString());
            valores.put(Utilidades.CAMPO_EMAIL_VISITANTE, email.getText().toString());
            valores.put(Utilidades.CAMPO_TELEFONO_VISITANTE, telefono.getText().toString());

            Long idRegistro = db.insert(Utilidades.TABLA_VISITANTE, Utilidades.CAMPO_ID_VISITANTE, valores);
            mensaje("Visitante ingresado con ID: " + idRegistro);
            limpiar();
            run.requestFocus();
        }
    }

    private boolean validarCamposVacios() {
        if (TextUtils.isEmpty(run.getText().toString().trim()) && run.getText().toString().trim().length() == 0){
            run.setError("Campo R.U.N es obligatorio");
            run.requestFocus();
        }else if (run.getText().toString().trim().length() <= 9){
            run.setError("RUN incorrecto");
            run.requestFocus();
        }else if (!validarRut(run.getText().toString())){
            run.setError("RUN no válido");
            run.requestFocus();
        }else if (TextUtils.isEmpty(nombre.getText().toString().trim()) && nombre.getText().toString().trim().length() == 0){
            nombre.setError("Campo Nombre es obligatorio");
            nombre.requestFocus();
        }else if (TextUtils.isEmpty(apellido.getText().toString().trim()) && apellido.getText().toString().trim().length() == 0){
            apellido.setError("Campo Apellido es obligatorio");
            apellido.requestFocus();
        }else if (TextUtils.isEmpty(email.getText().toString().trim()) && email.getText().toString().trim().length() == 0){
            email.setError("Campo Email es obligatorio");
            email.requestFocus();
        }else if (!validarEmail(email.getText().toString())){
            email.setError("Email no válido");
            email.requestFocus();
        }else if (TextUtils.isEmpty(telefono.getText().toString().trim()) && telefono.getText().toString().trim().length() == 0){
            telefono.setError("Campo Teléfono es obligatorio");
            telefono.requestFocus();
        }else {
            camposOK = true;
        }

        return camposOK;
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private void limpiar() {
        run.setText("");
        nombre.setText("");
        apellido.setText("");
        email.setText("");
        telefono.setText("");
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static boolean validarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }
}
