package com.duoc.franciscomora.mantenedores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainRegistroVisitasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_registro_visitas);
    }

    public void mostrarRegistrarVisita(View view) {
        Intent registrarVisita = new Intent(this, RegistrarVisitaActivity.class);
        startActivity(registrarVisita);
    }

    public void mostrarReportePorMuseo(View view) {
        Intent registrarVisita = new Intent(this, ReportePorMuseoActivity.class);
        startActivity(registrarVisita);
    }
}
