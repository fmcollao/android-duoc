package com.duoc.franciscomora.mantenedores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import database.ConexionHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConexionHelper conn = new ConexionHelper(this, "mantenedores" , null, 1);
    }

    public void mostrarMainMuseos(View view) {
        Intent mainMuseos = new Intent(this, MainMuseosActivity.class);
        startActivity(mainMuseos);
    }

    public void mostrarMainVisitantes(View view) {
        Intent mainVisitantes = new Intent(this, MainVisitantesActivity.class);
        startActivity(mainVisitantes);
    }

    public void mostrarMainReporteMuseos(View view) {
        Intent mainReporteMuseos = new Intent(this, MainReporteMuseosActivity.class);
        startActivity(mainReporteMuseos);
    }

    public void mostrarMainReporteVisitantes(View view) {
        Intent mainReporteVisitantes = new Intent(this, MainReporteVisitantesActivity.class);
        startActivity(mainReporteVisitantes);
    }

    public void mostrarMainRegistroVisitas(View view) {
        Intent mainRegistroVisitas = new Intent(this, MainRegistroVisitasActivity.class);
        startActivity(mainRegistroVisitas);
    }
}
