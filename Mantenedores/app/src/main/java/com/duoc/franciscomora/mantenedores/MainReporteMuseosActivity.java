package com.duoc.franciscomora.mantenedores;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

import database.ConexionHelper;
import utilidades.Utilidades;

public class MainReporteMuseosActivity extends AppCompatActivity {
    BarChart chart;
    ConexionHelper conn;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_reporte);
        conn = new ConexionHelper(this, "mantenedores" , null, 1);
        chart = (BarChart) findViewById(R.id.graficoReporteMuseos);
        graficar();
    }

    private void graficar() {
        db = conn.getReadableDatabase();
        long cantMuseos = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_MUSEO);
        //Datos
        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0, cantMuseos));

        BarDataSet set = new BarDataSet(entries, "Cantidad Museos");

        //Dibujar
        BarData dibuja = new BarData(set);
        chart.setData(dibuja);
        chart.setFitBars(true);
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setNoDataText("SIN DATOS");
        chart.invalidate();
    }
}
