package com.duoc.franciscomora.mantenedores;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import database.ConexionHelper;
import modelo.Museo;
import modelo.Visitante;
import utilidades.Utilidades;

public class ReportePorMuseoActivity extends AppCompatActivity {
    Spinner spMuseo;
    Spinner spVisitante;
    ArrayList<String> listadoMuseos;
    ArrayList<Museo> listaMuseo;
    ArrayList<String> listadoVisitantes;
    ArrayList<Visitante> listaVisitante;
    ConexionHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte_por_museo);
        conn = new ConexionHelper(this, "mantenedores" , null, 1);
        spMuseo = (Spinner) findViewById(R.id.spMuseo);
        spVisitante = (Spinner) findViewById(R.id.spVisitante);

        consultarListadoMuseos();
        consultarListadoVisitantes();

        ArrayAdapter<CharSequence> adaptadorMuseos = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listadoMuseos);
        spMuseo.setAdapter(adaptadorMuseos);
        ArrayAdapter<CharSequence> adaptadorVisitantes = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listadoVisitantes);
        spVisitante.setAdapter(adaptadorVisitantes);
    }

    private void consultarListadoVisitantes() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Visitante visitante = null;
        listaVisitante = new ArrayList<Visitante>();
        String sql = "SELECT * FROM " + Utilidades.TABLA_VISITANTE;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            visitante = new Visitante();
            visitante.setNombre(cursor.getString(2));
            visitante.setApellido(cursor.getString(3));

            listaVisitante.add(visitante);
        }

        obtenerListaVisitante();
    }

    private void obtenerListaVisitante() {
        listadoVisitantes = new ArrayList<String>();

        for (int i=0; i < listaVisitante.size();i++){
            listadoVisitantes.add(listaVisitante.get(i).getNombre() + " " + listaVisitante.get(i).getApellido());
        }
    }

    private void consultarListadoMuseos() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Museo museo = null;
        listaMuseo = new ArrayList<Museo>();
        String sql = "SELECT * FROM " + Utilidades.TABLA_MUSEO;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            museo = new Museo();
            museo.setNombre(cursor.getString(1));

            listaMuseo.add(museo);
        }

        obtenerListaMuseos();
    }

    private void obtenerListaMuseos() {
        listadoMuseos = new ArrayList<String>();

        for (int i=0; i < listaMuseo.size();i++){
            listadoMuseos.add(listaMuseo.get(i).getNombre());
        }
    }

    public void obtenerReportePorCantidad(View view) {
        SQLiteDatabase db = conn.getReadableDatabase();
        long cantMuseos = DatabaseUtils.queryNumEntries(db,Utilidades.TABLA_VISITAS,
                "nombre_museo=? AND nombre_visitante=?", new String[] {spMuseo.getSelectedItem().toString(),spVisitante.getSelectedItem().toString()});

        AlertDialog.Builder alertResultado = new AlertDialog.Builder(this);
        alertResultado.setIcon(R.mipmap.ic_launcher).setTitle("RESULTADO").setMessage("Cantidad de visitas: " + cantMuseos).setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alerta = alertResultado.create();
        alerta.show();
    }
}
