package com.duoc.franciscomora.mantenedores;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import database.ConexionHelper;
import utilidades.Utilidades;

public class RegistrarMuseoActivity extends AppCompatActivity {
    EditText nombre;
    EditText direccion;
    EditText web;
    EditText telefono;
    Boolean camposOK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_museo);

        nombre = (EditText) findViewById(R.id.txtNombreMuseo);
        direccion = (EditText) findViewById(R.id.txtDireccionMuseo);
        web = (EditText) findViewById(R.id.txtWebMuseo);
        telefono = (EditText) findViewById(R.id.txtTelefonoMuseo);
    }

    public void registrarMuseo(View view) {
        ConexionHelper conn = new ConexionHelper(this, "mantenedores" , null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();

        if (validarCamposVacios()){
            ContentValues valores = new ContentValues();
            valores.put(Utilidades.CAMPO_NOMBRE_MUSEO, nombre.getText().toString());
            valores.put(Utilidades.CAMPO_DIRECCION_MUSEO, direccion.getText().toString());
            valores.put(Utilidades.CAMPO_WEB_MUSEO, web.getText().toString());
            valores.put(Utilidades.CAMPO_TELEFONO_MUSEO, telefono.getText().toString());

            Long idRegistro = db.insert(Utilidades.TABLA_MUSEO, Utilidades.CAMPO_ID_MUSEO, valores);
            mensaje("Museo ingresado con ID: " + idRegistro);
            limpiar();
            nombre.requestFocus();
        }
    }

    private boolean validarCamposVacios() {
        if (TextUtils.isEmpty(nombre.getText().toString().trim()) && nombre.getText().toString().trim().length() == 0){
            nombre.setError("Campo Nombre es obligatorio");
            nombre.requestFocus();
        }else if (TextUtils.isEmpty(direccion.getText().toString().trim()) && direccion.getText().toString().trim().length() == 0){
            direccion.setError("Campo Dirección es obligatorio");
            direccion.requestFocus();
        }else if (TextUtils.isEmpty(web.getText().toString().trim()) && web.getText().toString().trim().length() == 0){
            web.setError("Campo Sitio Web es obligatorio");
            web.requestFocus();
        }else if (TextUtils.isEmpty(telefono.getText().toString().trim()) && telefono.getText().toString().trim().length() == 0){
            telefono.setError("Campo Teléfono es obligatorio");
            telefono.requestFocus();
        }else {
            camposOK = true;
        }

        return camposOK;
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private void limpiar() {
        nombre.setText("");
        direccion.setText("");
        web.setText("");
        telefono.setText("");
    }
}
