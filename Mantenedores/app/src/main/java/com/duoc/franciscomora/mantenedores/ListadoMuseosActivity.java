package com.duoc.franciscomora.mantenedores;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import database.ConexionHelper;
import modelo.Museo;
import utilidades.Utilidades;

public class ListadoMuseosActivity extends AppCompatActivity {
    ListView lstViewMuseos;
    ArrayList<String> listaInformacion;
    ArrayList<Museo> listaMuseos;
    ConexionHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_museos);
        conn = new ConexionHelper(this, "mantenedores" , null, 1);
        lstViewMuseos = (ListView) findViewById(R.id.listViewMuseos);
        listadoMuseos();
        ArrayAdapter adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaInformacion);
        lstViewMuseos.setAdapter(adaptador);
        mensaje("Ingrese nuevos MUSEOS para que se listen en el informe...");
    }

    private void listadoMuseos() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Museo museo = null;
        listaMuseos = new ArrayList<Museo>();
        String sql = "SELECT * FROM " + Utilidades.TABLA_MUSEO;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            museo = new Museo();

            museo.setId(cursor.getInt(0));
            museo.setNombre(cursor.getString(1));
            museo.setDireccion(cursor.getString(2));
            museo.setWeb(cursor.getString(3));
            museo.setTelefono(cursor.getString(4));

            listaMuseos.add(museo);
        }

        obtenerLista();
    }

    private void obtenerLista() {
        listaInformacion = new ArrayList<String>();

        for (int i = 0; i < listaMuseos.size(); i++){
            listaInformacion.add(listaMuseos.get(i).getId()+ "\n" + listaMuseos.get(i).getNombre() + "\n" + listaMuseos.get(i).getDireccion() + "\n" + listaMuseos.get(i).getWeb() + "\n" + listaMuseos.get(i).getTelefono());
        }
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
