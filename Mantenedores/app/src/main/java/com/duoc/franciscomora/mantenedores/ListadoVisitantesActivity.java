package com.duoc.franciscomora.mantenedores;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.ConexionHelper;
import modelo.Visitante;
import utilidades.Utilidades;

public class ListadoVisitantesActivity extends AppCompatActivity {
    ListView lstViewVisitantes;
    ArrayList<String> listaInformacion;
    ArrayList<Visitante> listaVisitantes;
    ConexionHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_visitantes);
        conn = new ConexionHelper(this, "mantenedores" , null, 1);
        lstViewVisitantes = (ListView) findViewById(R.id.lstViewVisitantes);
        listadoVisitantes();
        ArrayAdapter adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaInformacion);
        lstViewVisitantes.setAdapter(adaptador);
        mensaje("Ingrese nuevos VISITANTES para que se listen en el informe...");
    }

    private void listadoVisitantes() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Visitante visitante = null;
        listaVisitantes = new ArrayList<Visitante>();
        String sql = "SELECT * FROM " + Utilidades.TABLA_VISITANTE;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            visitante = new Visitante();

            visitante.setId(cursor.getInt(0));
            visitante.setRun(cursor.getString(1));
            visitante.setNombre(cursor.getString(2));
            visitante.setApellido(cursor.getString(3));
            visitante.setEmail(cursor.getString(4));
            visitante.setTelefono(cursor.getString(5));

            listaVisitantes.add(visitante);
        }

        obtenerLista();
    }

    private void obtenerLista() {
        listaInformacion = new ArrayList<String>();

        for (int i = 0; i < listaVisitantes.size(); i++){
            listaInformacion.add(listaVisitantes.get(i).getId()+ "\n" + listaVisitantes.get(i).getRun() + "\n" + listaVisitantes.get(i).getNombre() + "\n" + listaVisitantes.get(i).getApellido() + "\n" + listaVisitantes.get(i).getEmail() + "\n" + listaVisitantes.get(i).getTelefono());
        }
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
