package com.duoc.franciscomora.mantenedores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainMuseosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museos);
    }

    public void mostrarRegistroMuseo(View view) {
        Intent registroMuseos = new Intent(this, RegistrarMuseoActivity.class);
        startActivity(registroMuseos);
    }

    public void mostrarConsultarMuseo(View view) {
        Intent consultarMuseos = new Intent(this, ConsultaMuseoActivity.class);
        startActivity(consultarMuseos);
    }

    public void mostrarListadoMuseos(View view) {
        Intent listarMuseos = new Intent(this, ListadoMuseosActivity.class);
        startActivity(listarMuseos);
    }
}
