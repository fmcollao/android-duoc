package com.duoc.franciscomora.mantenedores;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import database.ConexionHelper;
import utilidades.Utilidades;

public class ConsultaVisitanteActivity extends AppCompatActivity {
    EditText id;
    EditText run;
    EditText nombre;
    EditText apellido;
    EditText email;
    EditText telefono;
    ConexionHelper conn;
    Boolean camposOK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_visitante);
        conn = new ConexionHelper(this, "mantenedores" , null, 1);

        id = (EditText) findViewById(R.id.txtIdVisitante);
        run = (EditText) findViewById(R.id.txtRunVisitante);
        nombre = (EditText) findViewById(R.id.txtNombreVisitante);
        apellido = (EditText) findViewById(R.id.txtApellidoVisitante);
        email = (EditText) findViewById(R.id.txtEmailVisitante);
        telefono = (EditText) findViewById(R.id.txtTelefonoVisitante);
        id.requestFocus();
    }

    public void buscarVisitante(View view) {
        SQLiteDatabase db = conn.getReadableDatabase();
        String[] parametros = {id.getText().toString()};
        String[] campos = {Utilidades.CAMPO_ID_VISITANTE,Utilidades.CAMPO_RUN_VISITANTE,Utilidades.CAMPO_NOMBRE_VISITANTE,Utilidades.CAMPO_APELLIDO_VISITANTE,Utilidades.CAMPO_EMAIL_VISITANTE,Utilidades.CAMPO_TELEFONO_VISITANTE};

        if (validarCamposVacios()){
            try {
                Cursor cursor = db.query(Utilidades.TABLA_VISITANTE, campos, Utilidades.CAMPO_ID_VISITANTE + "=?", parametros, null, null,null);
                cursor.moveToFirst();

                run.setText(cursor.getString(1));
                nombre.setText(cursor.getString(2));
                apellido.setText(cursor.getString(3));
                email.setText(cursor.getString(4));
                telefono.setText(cursor.getString(5));
                id.setEnabled(false);
                camposOK = false;

                cursor.close();
            }catch (Exception ex){
                mensaje("El Visitante No existe, intenta con otro ID!!!");
                limpiar();
            }
        }
    }

    public void actualizarVisitante(View view) {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {id.getText().toString()};

        if (validarCamposVacios()){
            ContentValues valores = new ContentValues();
            valores.put(Utilidades.CAMPO_RUN_VISITANTE, run.getText().toString());
            valores.put(Utilidades.CAMPO_NOMBRE_VISITANTE, nombre.getText().toString());
            valores.put(Utilidades.CAMPO_APELLIDO_VISITANTE, apellido.getText().toString());
            valores.put(Utilidades.CAMPO_EMAIL_VISITANTE, email.getText().toString());
            valores.put(Utilidades.CAMPO_TELEFONO_VISITANTE, telefono.getText().toString());

            db.update(Utilidades.TABLA_VISITANTE, valores,Utilidades.CAMPO_ID_VISITANTE + "=?", parametros);
            mensaje("Visitante " + nombre.getText().toString() + " " + apellido.getText().toString() + " con ID: " + id.getText().toString() + " actualizado!!!");
            id.setEnabled(true);
            id.requestFocus();
            limpiar();
            db.close();
            camposOK = false;
        }
    }

    public void eliminarVisitante(View view) {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {id.getText().toString()};

        if (validarCamposVacios()){
            db.delete(Utilidades.TABLA_VISITANTE, Utilidades.CAMPO_ID_VISITANTE + "=?", parametros);
            mensaje("Visitante Eliminado!!!");
            limpiar();
            db.close();
            camposOK = false;
        }
    }

    private void limpiar() {
        id.setText("");
        run.setText("");
        nombre.setText("");
        apellido.setText("");
        email.setText("");
        telefono.setText("");
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private boolean validarCamposVacios() {
        if (TextUtils.isEmpty(id.getText().toString().trim()) && id.getText().toString().trim().length() == 0){
            mensaje("El campo ID es obligatorio");
            id.requestFocus();
        }else {
            camposOK = true;
        }

        return camposOK;
    }
}
