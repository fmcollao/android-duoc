package com.duoc.franciscomora.mantenedores;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import database.ConexionHelper;
import utilidades.Utilidades;

public class ConsultaMuseoActivity extends AppCompatActivity {
    EditText id;
    EditText nombre;
    EditText direccion;
    EditText web;
    EditText telefono;
    ConexionHelper conn;
    boolean camposOK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_museo);

        conn = new ConexionHelper(this, "mantenedores" , null, 1);

        id = (EditText) findViewById(R.id.txtIdMuseo);
        nombre = (EditText) findViewById(R.id.txtNombreMuseo);
        direccion = (EditText) findViewById(R.id.txtDireccionMuseo);
        web = (EditText) findViewById(R.id.txtWebMuseo);
        telefono = (EditText) findViewById(R.id.txtTelefonoMuseo);
    }

    public void buscarMuseo(View view) {
        SQLiteDatabase db = conn.getReadableDatabase();
        String[] parametros = {id.getText().toString()};
        String[] campos = {Utilidades.CAMPO_ID_MUSEO,Utilidades.CAMPO_NOMBRE_MUSEO,Utilidades.CAMPO_DIRECCION_MUSEO,Utilidades.CAMPO_WEB_MUSEO,Utilidades.CAMPO_TELEFONO_MUSEO};

        if (validarCamposVacios()){
            try {
                Cursor cursor = db.query(Utilidades.TABLA_MUSEO, campos, Utilidades.CAMPO_ID_MUSEO + "=?", parametros, null, null,null);
                cursor.moveToFirst();

                nombre.setText(cursor.getString(1));
                direccion.setText(cursor.getString(2));
                web.setText(cursor.getString(3));
                telefono.setText(cursor.getString(4));
                id.setEnabled(false);
                camposOK = false;

                cursor.close();
            }catch (Exception ex){
                mensaje("El Museo No existe, intenta con otro ID!!!");
                limpiar();
            }
        }
    }

    public void actualizarMuseo(View view) {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {id.getText().toString()};

        if (validarCamposVacios()){
            ContentValues valores = new ContentValues();
            valores.put(Utilidades.CAMPO_NOMBRE_MUSEO, nombre.getText().toString());
            valores.put(Utilidades.CAMPO_DIRECCION_MUSEO, direccion.getText().toString());
            valores.put(Utilidades.CAMPO_WEB_MUSEO, web.getText().toString());
            valores.put(Utilidades.CAMPO_TELEFONO_MUSEO, telefono.getText().toString());

            db.update(Utilidades.TABLA_MUSEO, valores,Utilidades.CAMPO_ID_MUSEO + "=?", parametros);
            mensaje(nombre.getText().toString() + " con ID: " + id.getText().toString() + " actualizado!!!");
            id.setEnabled(true);
            id.requestFocus();
            limpiar();
            db.close();
            camposOK = false;
        }
    }

    public void eliminarMuseo(View view) {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {id.getText().toString()};

        if (validarCamposVacios()){
            db.delete(Utilidades.TABLA_MUSEO, Utilidades.CAMPO_ID_MUSEO + "=?", parametros);
            mensaje("Museo Eliminado!!!");
            limpiar();
            db.close();
            camposOK = false;
        }
    }

    private void limpiar() {
        id.setText("");
        nombre.setText("");
        direccion.setText("");
        web.setText("");
        telefono.setText("");
    }

    public void mensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private boolean validarCamposVacios() {
        if (TextUtils.isEmpty(id.getText().toString().trim()) && id.getText().toString().trim().length() == 0){
            mensaje("El campo ID es obligatorio");
            id.requestFocus();
        }else {
            camposOK = true;
        }

        return camposOK;
    }
}
