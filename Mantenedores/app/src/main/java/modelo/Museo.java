package modelo;

/**
 * Created by Francisco Mora on 06-10-2017.
 */

public class Museo {
    private Integer id;
    private String nombre;
    private String direccion;
    private String web;
    private String telefono;

    public Museo() {
    }

    public Museo(Integer id, String nombre, String direccion, String web, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.web = web;
        this.telefono = telefono;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
