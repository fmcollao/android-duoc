package database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import utilidades.Utilidades;

/**
 * Created by Francisco Mora on 06-10-2017.
 */

public class ConexionHelper extends SQLiteOpenHelper{

    public ConexionHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public ConexionHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utilidades.CREAR_TABLA_MUSEO);
        db.execSQL(Utilidades.CREAR_TABLA_VISITANTE);
        db.execSQL(Utilidades.CREAR_TABLA_VISITAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAntigua, int versionNueva) {
        String sql1 = "DROP TABLE IF EXIST museo";
        String sql2 = "DROP TABLE IF EXIST visitante";
        String sql3 = "DROP TABLE IF EXIST visitas";
        db.execSQL(sql1);
        db.execSQL(sql2);
        db.execSQL(sql3);
        onCreate(db);
    }
}
