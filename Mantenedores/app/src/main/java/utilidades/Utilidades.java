package utilidades;

/**
 * Created by Francisco Mora on 06-10-2017.
 */

public class Utilidades {
    //Constantes campos tabla museos
    public static final String TABLA_MUSEO = "museo";
    public static final String CAMPO_ID_MUSEO = "_id";
    public static final String CAMPO_NOMBRE_MUSEO = "nombre";
    public static final String CAMPO_DIRECCION_MUSEO = "direccion";
    public static final String CAMPO_WEB_MUSEO = "web";
    public static final String CAMPO_TELEFONO_MUSEO = "telefono";
    public static final String CREAR_TABLA_MUSEO = "CREATE TABLE "+ TABLA_MUSEO + "("
                                                    + CAMPO_ID_MUSEO +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                                                    + CAMPO_NOMBRE_MUSEO +" TEXT, "
                                                    + CAMPO_DIRECCION_MUSEO + " TEXT, "
                                                    + CAMPO_WEB_MUSEO + " TEX, "
                                                    + CAMPO_TELEFONO_MUSEO + " TEXT)";

    //Constantes campos tabla visitantes
    public static final String TABLA_VISITANTE = "visitante";
    public static final String CAMPO_ID_VISITANTE = "_id";
    public static final String CAMPO_RUN_VISITANTE = "run";
    public static final String CAMPO_NOMBRE_VISITANTE = "nombre";
    public static final String CAMPO_APELLIDO_VISITANTE = "apellido";
    public static final String CAMPO_EMAIL_VISITANTE = "email";
    public static final String CAMPO_TELEFONO_VISITANTE = "telefono";
    public static final String CREAR_TABLA_VISITANTE = "CREATE TABLE "+ TABLA_VISITANTE + "("
            + CAMPO_ID_VISITANTE +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CAMPO_RUN_VISITANTE +" TEXT, "
            + CAMPO_NOMBRE_VISITANTE + " TEXT, "
            + CAMPO_APELLIDO_VISITANTE + " TEX, "
            + CAMPO_EMAIL_VISITANTE + " TEX, "
            + CAMPO_TELEFONO_VISITANTE + " TEXT)";

    //Constantes campos tabla visitas
    public static final String TABLA_VISITAS = "visitas";
    public static final String CAMPO_ID_VISITAS = "_id";
    public static final String CAMPO_NOMBRE_MUSEO_REGISTRADO = "nombre_museo";
    public static final String CAMPO_NOMBRE_VISITANTE_REGISTRADO = "nombre_visitante";
    public static final String CAMPO_FECHA_VISITA = "fecha_visita";
    public static final String CAMPO_HORA_VISITA = "hora_visita";
    public static final String CREAR_TABLA_VISITAS = "CREATE TABLE "+ TABLA_VISITAS + "("
            + CAMPO_ID_VISITAS +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CAMPO_NOMBRE_MUSEO_REGISTRADO +" TEXT, "
            + CAMPO_NOMBRE_VISITANTE_REGISTRADO + " TEXT, "
            + CAMPO_FECHA_VISITA + " TEX, "
            + CAMPO_HORA_VISITA + " TEX)";
}
