package com.colecciones.casa.proyectocolecciones;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Iterator;

public class PantallaListado extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_listado);
    }

    public void listar(View view) {
        Intent intent = getIntent();
        ListadoCliente auxLista = (ListadoCliente) intent.getSerializableExtra("varListadoCliente");

        Iterator iter = auxLista.getListaCliente().iterator();

        String[] stringList = new String[auxLista.getListaCliente().size()];

        int posicion = 0;

        while (iter.hasNext()){
            Cliente auxCliente = new Cliente();
            auxCliente = (Cliente) iter.next();
            stringList[posicion] = auxCliente.getRut() + " " + auxCliente.getNombre();
            posicion++;
        }

        ListView auxListaView = (ListView) findViewById(R.id.listaClientes);

        auxListaView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stringList));
    }
}