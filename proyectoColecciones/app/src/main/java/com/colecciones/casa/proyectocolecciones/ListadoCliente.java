package com.colecciones.casa.proyectocolecciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Casa on 26-08-2017.
 */

public class ListadoCliente implements Serializable{
    private ArrayList<Cliente> listaCliente;

    public ListadoCliente() {
        this.setListaCliente(new ArrayList<Cliente>());
    }

    public void agregaCliente()
    {

    }

    public ArrayList<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(ArrayList<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }
}
