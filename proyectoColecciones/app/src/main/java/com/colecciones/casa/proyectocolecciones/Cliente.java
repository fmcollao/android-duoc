package com.colecciones.casa.proyectocolecciones;

import java.io.Serializable;

/**
 * Created by Casa on 26-08-2017.
 */

public class Cliente implements Serializable{
    private String rut;
    private String nombre;


    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
