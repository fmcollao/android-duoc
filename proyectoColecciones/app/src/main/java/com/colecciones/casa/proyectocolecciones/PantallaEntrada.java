package com.colecciones.casa.proyectocolecciones;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class PantallaEntrada extends AppCompatActivity {
    private ListadoCliente auxListaCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_entrada);
        this.setAuxListaCliente(new ListadoCliente());
    }

    public void guardar(View view) {
        try {
            EditText auxRun = (EditText) findViewById(R.id.txtRun);
            EditText auxNombre = (EditText) findViewById(R.id.txtNombre);

            Cliente auxCliente = new Cliente();
            auxCliente.setRut(auxRun.getText().toString());
            auxCliente.setNombre(auxNombre.getText().toString());

            this.getAuxListaCliente().agregaCliente();

        } catch(Exception ex){

        }

    }

    public void mostrar(View view) {
        Intent intent = new Intent(this, ListadoCliente.class);
        intent.putExtra("varListadoCliente", this.getAuxListadoCliente());
        startActivity(intent);
    }



    public void mensaje(String texto){
        Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
    }


    public ListadoCliente getAuxListaCliente() {
        return auxListaCliente;
    }

    public void setAuxListaCliente(ListadoCliente auxListaCliente) {
        this.auxListaCliente = auxListaCliente;
    }
}
