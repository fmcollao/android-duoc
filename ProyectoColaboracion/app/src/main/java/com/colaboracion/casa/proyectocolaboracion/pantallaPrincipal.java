package com.colaboracion.casa.proyectocolaboracion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class pantallaPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_principal);
    }

    public void enviar(View view)
    {
        EditText auxMensaje = (EditText) findViewById(R.id.txtMensaje);
        EditText auxRut = (EditText) findViewById(R.id.txtRUN);
        EditText auxNombre = (EditText) findViewById(R.id.txtNombre);

        Cliente auxCliente = new Cliente();
        auxCliente.setNombre(auxNombre.getText().toString());
        auxCliente.setRut(auxRut.getText().toString());

        //Se instancia la activity Mensaje
        Intent auxIntent = new Intent(this, Mensaje.class);
        auxIntent.putExtra("varMensaje", auxMensaje.getText().toString());
        auxIntent.putExtra("varCliente", auxCliente);
        startActivity(auxIntent);
    }
}
