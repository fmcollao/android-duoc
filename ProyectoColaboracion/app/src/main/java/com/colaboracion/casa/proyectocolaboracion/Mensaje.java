package com.colaboracion.casa.proyectocolaboracion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Mensaje extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensaje);
    }

    public void mostrar(View view) {
        EditText auxMensajeMostrar = (EditText) findViewById(R.id.txtMensajeMostrar);
        EditText auxRutMostrar = (EditText) findViewById(R.id.txtRUNMostrar);
        EditText auxNombreMostrar = (EditText) findViewById(R.id.txtNombreMostrar);

        Cliente auxClienteMostrar = new Cliente();
        auxClienteMostrar = (Cliente) getIntent().getSerializableExtra("varCliente");
        auxRutMostrar.setText(auxClienteMostrar.getRut());
        auxNombreMostrar.setText(auxClienteMostrar.getNombre());
        auxMensajeMostrar.setText(getIntent().getStringExtra("varMensaje"));
    }
}
