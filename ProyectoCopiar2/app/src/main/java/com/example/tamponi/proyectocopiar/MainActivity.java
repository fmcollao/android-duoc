package com.example.tamponi.proyectocopiar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void metodoCopiar(View v)
    {
        EditText auxOriginal = (EditText)findViewById(R.id.txtOriginal);
        EditText auxCopiar = (EditText)findViewById(R.id.txtCopia);

        auxCopiar.setText(auxOriginal.getText().toString());

    }


}
