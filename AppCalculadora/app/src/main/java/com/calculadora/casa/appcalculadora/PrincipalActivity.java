package com.calculadora.casa.appcalculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class PrincipalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
    }

    public void Suma(View vista){

        EditText auxNumero1 = (EditText) findViewById(R.id.txtNumero1);
        EditText auxNumero2 = (EditText) findViewById(R.id.txtNumero2);
        EditText auxResultado = (EditText) findViewById(R.id.txtResultado);

        OperacionesMatemáticas auxOperacion = new OperacionesMatemáticas();

        auxOperacion.setNumero1(Double.parseDouble(auxNumero1.getText().toString()));
        auxOperacion.setNumero2(Double.parseDouble(auxNumero2.getText().toString()));

        auxResultado.setText(String.valueOf(auxOperacion.Suma()));
    }

    public void Division(View vista){

        EditText auxNumero1 = (EditText) findViewById(R.id.txtNumero1);
        EditText auxNumero2 = (EditText) findViewById(R.id.txtNumero2);
        EditText auxResultado = (EditText) findViewById(R.id.txtResultado);

        if (Double.parseDouble(auxNumero2.getText().toString()) == 0){
            this.Mensaje("El número 2 no puede ser igual a cero");
        }else {
            OperacionesMatemáticas auxOperacion = new OperacionesMatemáticas();

            auxOperacion.setNumero1(Double.parseDouble(auxNumero1.getText().toString()));
            auxOperacion.setNumero2(Double.parseDouble(auxNumero2.getText().toString()));

            auxResultado.setText(String.valueOf(auxOperacion.Division()));
        }
    }

    public void Resta(View vista){

        EditText auxNumero1 = (EditText) findViewById(R.id.txtNumero1);
        EditText auxNumero2 = (EditText) findViewById(R.id.txtNumero2);
        EditText auxResultado = (EditText) findViewById(R.id.txtResultado);

        OperacionesMatemáticas auxOperacion = new OperacionesMatemáticas();

        auxOperacion.setNumero1(Double.parseDouble(auxNumero1.getText().toString()));
        auxOperacion.setNumero2(Double.parseDouble(auxNumero2.getText().toString()));

        auxResultado.setText(String.valueOf(auxOperacion.Resta()));
    }

    public void Multiplicacion(View vista){

        EditText auxNumero1 = (EditText) findViewById(R.id.txtNumero1);
        EditText auxNumero2 = (EditText) findViewById(R.id.txtNumero2);
        EditText auxResultado = (EditText) findViewById(R.id.txtResultado);

        OperacionesMatemáticas auxOperacion = new OperacionesMatemáticas();

        auxOperacion.setNumero1(Double.parseDouble(auxNumero1.getText().toString()));
        auxOperacion.setNumero2(Double.parseDouble(auxNumero2.getText().toString()));

        auxResultado.setText(String.valueOf(auxOperacion.Multiplicacion()));
    }

    public void Mensaje(String texto){
        Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
    }
}
