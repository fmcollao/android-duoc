package com.calculadora.casa.appcalculadora;

/**
 * Created by Casa on 19-08-2017.
 */

public class OperacionesMatemáticas {
    private double numero1;
    private double numero2;

    public double Suma(){
        return this.getNumero1() + this.getNumero2();
    }

    public double Resta(){
        return this.getNumero1() - this.getNumero2();
    }

    public double Multiplicacion(){
        return this.getNumero1() * this.getNumero2();
    }

    public double Division(){
        return this.getNumero1() / this.getNumero2();
    }

    public double getNumero1() {
        return numero1;
    }

    public void setNumero1(double numero1) {
        this.numero1 = numero1;
    }

    public double getNumero2() {
        return numero2;
    }

    public void setNumero2(double numero2) {
        this.numero2 = numero2;
    }
}
